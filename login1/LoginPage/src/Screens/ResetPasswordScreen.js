import { View, Text ,StyleSheet} from 'react-native'
import React,  { useState } from 'react'
import Header from '../components/Header'
import Paragraph from '../components/Paragraph'
import Button from '../components/Button'
import Background from '../components/Background'
import Logo from '../components/Logo'
import TextInput from '../components/TextInput'
import { emailValidator } from '../core/helpers/emailValidator'
import { passwordValidator } from '../core/helpers/passwordValidator'
import BackButton from '../components/BackButton'
import { TouchableOpacity } from 'react-native'
import { theme } from '../core/theme'



export default function ResetPasswordScreen({navigation}) {
    const [email, setEmail]= useState({value:"", error: ""})
    

    const onSubmitPressed=()=>{
        const emailError= emailValidator(email.value);
        if (emailError || passwordError){
            setEmail({...email, error: emailError });
        }
    }
  return (
    <Background>
       <BackButton goBack={navigation.goBack}/> 
      <Logo/>
      <Header >Restore Password</Header>
      <TextInput 
      label="Email"
      value={email.value}
      error={email.error}
      errorText={email.error}
      onChangeText={(text)=> setEmail({ value: text, error:""})}
      description="You will receive email with password reset link."/>
     
      <Button mode="contained" onPress= {onSubmitPressed} >Send Instruction</Button>
     
  

    </Background>
      
    
  )
}

const styles= StyleSheet.create({
  row: {
      flexDirection: 'row',
      marginTop: 4,
  },
  link: {
      fontWeight: 'bold',
      color: theme.colors.primary,
  }
})
