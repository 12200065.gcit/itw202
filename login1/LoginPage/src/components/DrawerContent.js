import { DrawerContentScrollView, DrawerItem } from '@react-navigation/drawer'
import React from 'react'
import {View, StyleSheet} from 'react-native'
import { Text, Avatar, Title, Caption, Paragraph,Drawer,TouchableRipple,Switch} from 'react-native-paper'

export default function DrawerContent(){
    return(
        <DrawerContentScrollView>
            <View style ={styles.drawerContent}>
                <View style ={styles.userInfoSection}>
                    <Avatar.Image
                    size={80}
                    source={
                        require("../../assets/namgay.png")
                    }/><Title style ={styles.title}> Namgay Wangchuk</Title>
                    <Caption style ={styles.caption}>@NamgayGCIT</Caption>
                    <View style ={styles.row}>
                        <View style ={styles.section}>
                            <Paragraph style ={[styles.paragraph, styles.caption]}> 54 </Paragraph>
                            <Caption style ={styles.caption}>Following</Caption>
                        </View>
                    </View>
                </View>
                <Drawer.Section style ={styles.drawerSection}>
                <DrawerItem 
                label= " Setting"
                onPress={()=>{}}></DrawerItem>
            </Drawer.Section>
            <Drawer.Section title="Prefrences">
                <TouchableRipple onPress={()=>{}}>
                    <View style ={styles.prefrences}>
                        <Text>Notification</Text>
                        <View pointerEvents="none">
                            <Switch value={false}/>
                        </View>
                    </View>
                </TouchableRipple>
            </Drawer.Section>
            </View>
    
        
        </DrawerContentScrollView>
    )
}
const styles= StyleSheet.create({
    drawerContent: {
        flex: 1,
    },
    userInfoSection: {
        paddingLeft: 20,
    },
    title: {
        marginTop: 20,
        fontWeight: 'bold',
    },
    caption: {
        fontSize: 14,
        lineHeight: 14,
    },
    row: {
        marginTop: 20,
        flexDirection: 'row',
        alignItems: 'center',
    },
    section: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 15,
    },
    paragraph: {
        fontWeight: 'bold',
        marginRight: 3,
    
    },
    drawerSection: {
        marginTop: 15,
    },
    prefrences: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 12,
        paddingHorizontal: 16,
    },
})
