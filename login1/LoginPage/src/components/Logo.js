import { View, Text, Image, StyleSheet } from 'react-native'
import React from 'react'

export default function Logo() {
  return  <Image 
  style={styles.image}
  source={require('../../assets/logo.png')}>
      
  </Image>;
}

const styles= StyleSheet.create({
    image: {
        width: 110,
        height: 110,
        marginBottom: 8,
    },
});