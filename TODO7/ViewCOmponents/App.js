import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <View style = {styles.box1}></View>
      <View style = {styles.box2}></View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
      flex: 1,
      alignItems: 'center',
      justifyContent:'center'
  },
  box1: {
    backgroundColor: 'red',
    width: 100,
      height: 100,

  },
  box2: {
    backgroundColor: 'blue',
    width: 100,
      height: 100,

  }
});
