import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { add, multiply } from './components/nameExport';
import division from './components/defaultExport';
import React from 'react';
import Courses from './components/funcComponents';

// export default function App() {
//   return (
//     <View style={styles.container}>
//       <Text>Open up App.js to start working on your app!</Text>
//       <Text>Result of addition:{add(5,6)}</Text>
//       <Text>Result of multiplication:{multiply(5,8)}</Text>
//       <Text>Result of division:{division(10,2)}</Text>

//       <StatusBar style="auto" />
//     </View>
//   );
// }
// export default class App extends React.Component{
//   render = () => (
//     <View style={styles.container}>
//       <Text>Practical 2</Text>
//       <Text>Stateless and Statefull components</Text>
//       <Text style={styles.text}>
//         You are ready to start the journey.
//       </Text>
//     </View>
//   )
// }
export default function App(){
  return(
    <View style={styles.container}>{}
      <Courses></Courses>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
