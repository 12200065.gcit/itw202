import { StyleSheet, View} from 'react-native';
import StartGameScreen from './Screens/StartGameScreen';
import { LinearGradient } from 'expo-linear-gradient';
import { useState } from 'react';
import GameScreen from './Screens/GameScreen';
import { SafeAreaView } from 'react-native';
import Colors from './constants/Colors';

export default function App() {
  const [userNumber,setUserNumber] = useState()

  function pickedNumberHandler(pickerNumber){
    setUserNumber(pickerNumber)
  }
  let screen = <StartGameScreen onPickNumber = {pickedNumberHandler}/>
  if (userNumber){
    screen= <GameScreen UserNumber={userNumber}/>
  }

  return (
    <LinearGradient 
    style={styles.container}
    colors={[Colors.primary700, Colors.accent500]}>
      <SafeAreaView  style={styles.container}>{screen}</SafeAreaView>
    
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  container: {
   flex:1,
  },
});
