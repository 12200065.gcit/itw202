import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import Title from '../components/ui/Title'
import { useState } from 'react';
import NumberContainer from '../components/game/NumberContainer';
import PrimaryButton from '../components/ui/PrimaryButton';

export default function GameScreen(){
function generateRandomBetween(min, max, exclude){
    const rndNum = Math.floor(Math.random()* (max-min))+ min;
    
    if (rndNum == exclude){
        return generateRandomBetween(min, max, exclude)
    }
    else{
        return rndNum;
    }
}
const GameScreen= ({UserNumber}) => {
    const initialGuess = generateRandomBetween(1,100, UserNumber)
    const [ currentGuess, setCurrentGuess]= useState(initialGuess)

}

  return (
    <View style={styles.screen}>
        <View >
            <Title>Opponents Guess</Title> 
            <NumberContainer>24</NumberContainer>
        </View>
        
        <View>
            <Text> Higher or Lower?</Text>
            <PrimaryButton>+</PrimaryButton>
            <PrimaryButton>-</PrimaryButton>
        </View>
    </View>

  )
}



const styles = StyleSheet.create({
    screen:{
        flex: 1,
        padding: 24
    }
   
})