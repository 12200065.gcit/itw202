import React, {useState} from 'react'
import {TextInput, View, StyleSheet, Alert} from 'react-native'
import { Colors } from 'react-native/Libraries/NewAppScreen';
import PrimaryButton from '../components/ui/PrimaryButton'
function StartGameScreen( {onPickNumber}) {
  const [enteredNumber, setEnteredNumber] = useState('');
  function numberInputHandler (enteredText){
    setEnteredNumber(enteredText);
  }
  function resetInputHandler( ){
    setEnteredNumber('')
  }
  function confrimInputHandler(){
    const chosenNumber = parseInt(enteredNumber)
    if (isNaN (chosenNumber) || chosenNumber < 1 || chosenNumber > 99){
      Alert.alert('Invilad number!', 'Number has to be anumber between 1 and 90',
      [{text: 'okay', style: 'destructive', onPress: resetInputHandler}]
      )
      return;
    }
    // console.log(chosenNumber)
    onPickNumber(chosenNumber)
  }
  return (
    <View style={styles.inputContainer}>
        <TextInput
         style={styles.numberInput}
         keyboardType='number-pad'
         maxLength={2}
         autoCapitalize='none'
         autoCorrect={false}
         value= {enteredNumber}
         onChangeText= {numberInputHandler}
         />
         <View style={styles.buttonsContainer}>
           <View style={styles.buttonContainer}>
              <PrimaryButton onPress={resetInputHandler}>Reset</PrimaryButton>
           </View>
           <View style={styles.buttonContainer}>
           <PrimaryButton onPress={confrimInputHandler}>Confirm</PrimaryButton>
           </View>
         </View>
    </View>
  )
}
export default StartGameScreen
const styles = StyleSheet.create({
  inputContainer: {
    justifyContent: 'center',
    alignItems:'center',
    marginTop: 100,
    marginHorizontal: 24,
    padding: 16,
    backgroundColor: Colors.primary500,
    borderRadius: 8,
    elevation: 4,
    shadowColor: 'black',
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 6,
    shadowOpacity: 0.25
  },
  numberInput: {
    height: 50,
    width: 50,
    fontSize: 32,
    borderBottomColor:Colors.accent500,
    borderBottomWidth: 2,
    color:Colors.accent500,
    marginVertical: 8,
    fontWeight: 'bold',
    textAlign: 'center'
},
buttonsContainer: {
    flexDirection: 'row'
},
buttonContainer: {
    flex: 1,
}
})