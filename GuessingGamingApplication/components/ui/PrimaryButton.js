import { View, Text, StyleSheet, Pressable } from 'react-native'
import React from 'react'


export default function PrimaryButton({children, onPress}) {
  return (
    <View style={styles.buttonOutercontainer}>
      <Pressable android_ripple={{color: 'yellow'}} 
        style={({pressed}) => pressed ?
        [styles.buttonInnercontainer, styles.pressed]:
        styles.buttonInnercontainer} onPress={onPress}>
        <Text style={styles.S2}>{children}</Text>
     </Pressable>
    </View>
   
  )
}
const styles = StyleSheet.create({
    buttonOutercontainer: {
      borderRadius: 28,
      margin: 4,
      overflow: 'hidden',
    },
    buttonInnercontainer: {
        backgroundColor: '#72063c',
        borderRadius: 28,
        paddingVertical: 16,
        elevation: 2,
        margin: 4,
      },
    S2: {
        color: 'white',
        textAlign: 'center',
    },
    pressed: {
        opacity: 0.75,
    }
  });
