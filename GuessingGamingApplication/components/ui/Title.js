import { View, Text, StyleSheet } from 'react-native'
import React from 'react'
import Colors from '../../constants/Colors'

export default function Title({children}) {
  return (
    <View>
      <Text style={styles.title}>{children}</Text>
    </View>
  )
}
const styles=StyleSheet.create({
    title: {
        borderWidth:2,
        borderColor:'white',
        fontSize:24,
        textAlign:'center',
        fontWeight:'bold',
        color:'white',
        padding:12
    }
})