import React from "react";
import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container: {
        flex:1,
        alignItems: 'flex-start',
        justifyContent: 'center',
        backgroundColor: '#fff',
    },
    text1: {
        fontSize: 16,
        
    },
    text2: {
        fontSize:16,
        fontWeight: 'bold',

    }
});

export default styles;
