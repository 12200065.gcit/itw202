import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import ButtonComponent from './components/buttonComponent.js';

export default function App (){
  return(
    <View style = {styles.container}>
    <ButtonComponent/> 
    </View>
  );
}
const styles = StyleSheet.create({
  container: { 
    flex: 1,
    justifyContent: "center",
    paddingHorizontal: 10
  },
})

