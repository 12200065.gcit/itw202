import React, {useState} from 'react';
import {View, Text, Button, StyleSheet} from 'react-native';

const ButtonComponent =() => {
  const [count, setCount] = useState(0);

    return ( 
      <View style={styles.container}>
      <Text style={styles.text}>The button was pressed {count} times! </Text>
      <Button 
      title='PRESS ME' 
      disabled={count>=3} 
      onPress={()=> setCount(count + 1)}
      /> 
      </View> 
    
  );
};
const styles = StyleSheet.create({
  container: { 
    flex: 1,
    justifyContent: "center",
  },
  text: {
    justifyContent: "center",
   
  },
});

export default ButtonComponent;

