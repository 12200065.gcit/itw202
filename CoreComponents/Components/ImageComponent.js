import React from 'react';
import {View, Image, StyleSheet} from 'react-native';
const ImageComponent =()=> {
    return(
        <View>
            <Image 
            style={styles.logoContain}
            source ={require('../assets/favicon.png')}
            />
                <Image
                style={styles.logoStretch}
                source={{uri: 'https://picsum.photos/100/100'}}
                />
        </View>
    );
}
export default ImageComponent;

const styles= StyleSheet.create({
    logoContain: {
        width:200, height: 100,
        resizeMode: 'contain',
    },
    logoStretch:{
        width: 200, heigth: 100,
        resizeMode: 'stretch',
    },
});