import React from "react";
import { Text, View, Image, StyleSheet } from 'react-native';

var Img = ()=>{
    return(
        <View>
            <Image
           
            source={{uri: 'https://picsum.photos/100/100',height:100,width:100}}>

            </Image >
            <Image 
            style={styles.p1}
            source={require('../assets/logo-og.png')}>
                
            </Image>
        </View>
    )
};
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    p1: {
        width:100,
        height:100
    }
})
export default Img;
