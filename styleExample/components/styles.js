import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'flex-start',
      justifyContent: 'flex-start',
      flexDirection: 'row',
      marginTop: 50,
      marginLeft: 60
    },
    box1:{
      backgroundColor: 'red',
      width: 45,
      height: 150,
      borderColor: 'black',
    },
    box2:{
      backgroundColor: 'blue',
      width: 105,
      height: 150,
      borderColor: 'black',
    },
    box3:{
      backgroundColor: 'green',
      width: 7,
      height: 150,
      borderColor: 'black',
    }
  });
  export default styles;
  