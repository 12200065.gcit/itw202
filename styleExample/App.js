import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import styles from './components/styles.js';

export default function App() {
  return ( 

    <View style = {styles.container} >

    <View style = {styles.box1}> 
    <Text style = {{textAlign:'center', marginTop: 63}}>1</Text>
    </View>

    <View style = {styles.box2}>
    <Text style = {{textAlign:'center', marginTop: 63}}>2</Text>
    </View>
 
    <View style = {styles.box3}>
    <Text style = {{textAlign:'center', marginTop: 63}}>3</Text>
    </View>

    </View>
  );
}